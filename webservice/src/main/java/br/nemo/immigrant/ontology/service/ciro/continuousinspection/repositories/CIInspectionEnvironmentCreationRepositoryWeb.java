package br.nemo.immigrant.ontology.service.ciro.continuousinspection.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.models.CIInspectionEnvironmentCreation;
import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.repositories.CIInspectionEnvironmentCreationRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "ciinspectionenvironmentcreation", path = "ciinspectionenvironmentcreation")
public interface CIInspectionEnvironmentCreationRepositoryWeb extends CIInspectionEnvironmentCreationRepository {

}
