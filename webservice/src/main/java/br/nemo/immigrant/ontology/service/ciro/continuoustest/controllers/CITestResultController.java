package br.nemo.immigrant.ontology.service.ciro.continuoustest.controllers;

import br.nemo.immigrant.ontology.entity.ciro.continuoustest.models.CITestResult;
import br.nemo.immigrant.ontology.entity.ciro.continuoustest.repositories.CITestResultRepository;
import br.nemo.immigrant.ontology.service.ciro.continuoustest.records.CITestResultInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class CITestResultController  {

  @Autowired
  CITestResultRepository repository;

  @QueryMapping
  public List<CITestResult> findAllCITestResults() {
    return repository.findAll();
  }

  @QueryMapping
  public CITestResult findByIDCITestResult(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public CITestResult createCITestResult(@Argument CITestResultInput input) {
    CITestResult instance = CITestResult.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public CITestResult updateCITestResult(@Argument Long id, @Argument CITestResultInput input) {
    CITestResult instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("CITestResult not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteCITestResult(@Argument Long id) {
    repository.deleteById(id);
  }

}
