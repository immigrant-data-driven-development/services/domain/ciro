package br.nemo.immigrant.ontology.service.ciro.continuousbuild.controllers;

import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.TestCodeCopy;
import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.repositories.TestCodeCopyRepository;
import br.nemo.immigrant.ontology.service.ciro.continuousbuild.records.TestCodeCopyInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class TestCodeCopyController  {

  @Autowired
  TestCodeCopyRepository repository;

  @QueryMapping
  public List<TestCodeCopy> findAllTestCodeCopys() {
    return repository.findAll();
  }

  @QueryMapping
  public TestCodeCopy findByIDTestCodeCopy(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public TestCodeCopy createTestCodeCopy(@Argument TestCodeCopyInput input) {
    TestCodeCopy instance = TestCodeCopy.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public TestCodeCopy updateTestCodeCopy(@Argument Long id, @Argument TestCodeCopyInput input) {
    TestCodeCopy instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("TestCodeCopy not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteTestCodeCopy(@Argument Long id) {
    repository.deleteById(id);
  }

}
