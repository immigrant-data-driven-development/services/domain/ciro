package br.nemo.immigrant.ontology.service.ciro.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.*;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = {"br.nemo.immigrant.ontology.service.ciro.*"})
@EntityScan(basePackages = {"br.nemo.immigrant.ontology.entity.*"})
@EnableJpaRepositories(basePackages = {"br.nemo.immigrant.ontology.service.ciro.*"})
@OpenAPIDefinition(info = @Info(
  title = "Continuous Integration Reference Ontology WebService",
  version = "1.0",
  description = "Continuous Integration Reference Ontology (CIRO) consolidates reference literature about Continuous Integration (CI)."))

public class Application {

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }
}
