package br.nemo.immigrant.ontology.service.ciro.continuousbuild.controllers;

import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.BuildEnviromentCreation;
import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.repositories.BuildEnviromentCreationRepository;
import br.nemo.immigrant.ontology.service.ciro.continuousbuild.records.BuildEnviromentCreationInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class BuildEnviromentCreationController  {

  @Autowired
  BuildEnviromentCreationRepository repository;

  @QueryMapping
  public List<BuildEnviromentCreation> findAllBuildEnviromentCreations() {
    return repository.findAll();
  }

  @QueryMapping
  public BuildEnviromentCreation findByIDBuildEnviromentCreation(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public BuildEnviromentCreation createBuildEnviromentCreation(@Argument BuildEnviromentCreationInput input) {
    BuildEnviromentCreation instance = BuildEnviromentCreation.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public BuildEnviromentCreation updateBuildEnviromentCreation(@Argument Long id, @Argument BuildEnviromentCreationInput input) {
    BuildEnviromentCreation instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("BuildEnviromentCreation not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteBuildEnviromentCreation(@Argument Long id) {
    repository.deleteById(id);
  }

}
