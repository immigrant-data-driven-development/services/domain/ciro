package br.nemo.immigrant.ontology.service.ciro.continuousinspection.controllers;

import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.models.AutomatedArtifactInspection;
import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.repositories.AutomatedArtifactInspectionRepository;
import br.nemo.immigrant.ontology.service.ciro.continuousinspection.records.AutomatedArtifactInspectionInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class AutomatedArtifactInspectionController  {

  @Autowired
  AutomatedArtifactInspectionRepository repository;

  @QueryMapping
  public List<AutomatedArtifactInspection> findAllAutomatedArtifactInspections() {
    return repository.findAll();
  }

  @QueryMapping
  public AutomatedArtifactInspection findByIDAutomatedArtifactInspection(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public AutomatedArtifactInspection createAutomatedArtifactInspection(@Argument AutomatedArtifactInspectionInput input) {
    AutomatedArtifactInspection instance = AutomatedArtifactInspection.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public AutomatedArtifactInspection updateAutomatedArtifactInspection(@Argument Long id, @Argument AutomatedArtifactInspectionInput input) {
    AutomatedArtifactInspection instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("AutomatedArtifactInspection not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteAutomatedArtifactInspection(@Argument Long id) {
    repository.deleteById(id);
  }

}
