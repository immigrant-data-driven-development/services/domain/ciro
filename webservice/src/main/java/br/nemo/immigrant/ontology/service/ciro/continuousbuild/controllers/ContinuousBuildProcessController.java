package br.nemo.immigrant.ontology.service.ciro.continuousbuild.controllers;

import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.ContinuousBuildProcess;
import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.repositories.ContinuousBuildProcessRepository;
import br.nemo.immigrant.ontology.service.ciro.continuousbuild.records.ContinuousBuildProcessInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ContinuousBuildProcessController  {

  @Autowired
  ContinuousBuildProcessRepository repository;

  @QueryMapping
  public List<ContinuousBuildProcess> findAllContinuousBuildProcesss() {
    return repository.findAll();
  }

  @QueryMapping
  public ContinuousBuildProcess findByIDContinuousBuildProcess(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public ContinuousBuildProcess createContinuousBuildProcess(@Argument ContinuousBuildProcessInput input) {
    ContinuousBuildProcess instance = ContinuousBuildProcess.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public ContinuousBuildProcess updateContinuousBuildProcess(@Argument Long id, @Argument ContinuousBuildProcessInput input) {
    ContinuousBuildProcess instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("ContinuousBuildProcess not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteContinuousBuildProcess(@Argument Long id) {
    repository.deleteById(id);
  }

}
