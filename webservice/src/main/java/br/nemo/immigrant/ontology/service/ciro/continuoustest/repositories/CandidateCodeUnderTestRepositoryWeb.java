package br.nemo.immigrant.ontology.service.ciro.continuoustest.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuoustest.models.CandidateCodeUnderTest;
import br.nemo.immigrant.ontology.entity.ciro.continuoustest.repositories.CandidateCodeUnderTestRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "candidatecodeundertest", path = "candidatecodeundertest")
public interface CandidateCodeUnderTestRepositoryWeb extends CandidateCodeUnderTestRepository {

}
