package br.nemo.immigrant.ontology.service.ciro.continuoustest.controllers;

import br.nemo.immigrant.ontology.entity.ciro.continuoustest.models.CITestingEnvironmentCreation;
import br.nemo.immigrant.ontology.entity.ciro.continuoustest.repositories.CITestingEnvironmentCreationRepository;
import br.nemo.immigrant.ontology.service.ciro.continuoustest.records.CITestingEnvironmentCreationInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class CITestingEnvironmentCreationController  {

  @Autowired
  CITestingEnvironmentCreationRepository repository;

  @QueryMapping
  public List<CITestingEnvironmentCreation> findAllCITestingEnvironmentCreations() {
    return repository.findAll();
  }

  @QueryMapping
  public CITestingEnvironmentCreation findByIDCITestingEnvironmentCreation(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public CITestingEnvironmentCreation createCITestingEnvironmentCreation(@Argument CITestingEnvironmentCreationInput input) {
    CITestingEnvironmentCreation instance = CITestingEnvironmentCreation.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public CITestingEnvironmentCreation updateCITestingEnvironmentCreation(@Argument Long id, @Argument CITestingEnvironmentCreationInput input) {
    CITestingEnvironmentCreation instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("CITestingEnvironmentCreation not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteCITestingEnvironmentCreation(@Argument Long id) {
    repository.deleteById(id);
  }

}
