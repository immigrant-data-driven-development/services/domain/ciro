package br.nemo.immigrant.ontology.service.ciro.continuousintegration.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousintegration.models.ContinuousIntegrationServer;
import br.nemo.immigrant.ontology.entity.ciro.continuousintegration.repositories.ContinuousIntegrationServerRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "continuousintegratioserver", path = "continuousintegratioserver")
public interface ContinuousIntegrationServerRepositoryWeb extends ContinuousIntegrationServerRepository {

}
