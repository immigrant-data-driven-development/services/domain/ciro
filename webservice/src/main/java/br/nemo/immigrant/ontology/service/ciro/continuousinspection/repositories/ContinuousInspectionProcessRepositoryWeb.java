package br.nemo.immigrant.ontology.service.ciro.continuousinspection.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.models.ContinuousInspectionProcess;
import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.repositories.ContinuousInspectionProcessRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "continuousinspectionprocess", path = "continuousinspectionprocess")
public interface ContinuousInspectionProcessRepositoryWeb extends ContinuousInspectionProcessRepository {

}
