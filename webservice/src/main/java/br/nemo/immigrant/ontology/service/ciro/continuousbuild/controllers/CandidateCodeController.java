package br.nemo.immigrant.ontology.service.ciro.continuousbuild.controllers;

import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.CandidateCode;
import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.repositories.CandidateCodeRepository;
import br.nemo.immigrant.ontology.service.ciro.continuousbuild.records.CandidateCodeInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class CandidateCodeController  {

  @Autowired
  CandidateCodeRepository repository;

  @QueryMapping
  public List<CandidateCode> findAllCandidateCodes() {
    return repository.findAll();
  }

  @QueryMapping
  public CandidateCode findByIDCandidateCode(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public CandidateCode createCandidateCode(@Argument CandidateCodeInput input) {
    CandidateCode instance = CandidateCode.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public CandidateCode updateCandidateCode(@Argument Long id, @Argument CandidateCodeInput input) {
    CandidateCode instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("CandidateCode not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteCandidateCode(@Argument Long id) {
    repository.deleteById(id);
  }

}
