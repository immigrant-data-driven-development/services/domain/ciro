package br.nemo.immigrant.ontology.service.ciro.continuoustest.controllers;

import br.nemo.immigrant.ontology.entity.ciro.continuoustest.models.CandidateCodeUnderTest;
import br.nemo.immigrant.ontology.entity.ciro.continuoustest.repositories.CandidateCodeUnderTestRepository;
import br.nemo.immigrant.ontology.service.ciro.continuoustest.records.CandidateCodeUnderTestInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class CandidateCodeUnderTestController  {

  @Autowired
  CandidateCodeUnderTestRepository repository;

  @QueryMapping
  public List<CandidateCodeUnderTest> findAllCandidateCodeUnderTests() {
    return repository.findAll();
  }

  @QueryMapping
  public CandidateCodeUnderTest findByIDCandidateCodeUnderTest(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public CandidateCodeUnderTest createCandidateCodeUnderTest(@Argument CandidateCodeUnderTestInput input) {
    CandidateCodeUnderTest instance = CandidateCodeUnderTest.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public CandidateCodeUnderTest updateCandidateCodeUnderTest(@Argument Long id, @Argument CandidateCodeUnderTestInput input) {
    CandidateCodeUnderTest instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("CandidateCodeUnderTest not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteCandidateCodeUnderTest(@Argument Long id) {
    repository.deleteById(id);
  }

}
