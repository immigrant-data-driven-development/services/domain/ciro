package br.nemo.immigrant.ontology.service.ciro.continuousinspection.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.models.AutomatedArtifactInspection;
import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.repositories.AutomatedArtifactInspectionRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "automatedartifactinspection", path = "automatedartifactinspection")
public interface AutomatedArtifactInspectionRepositoryWeb extends AutomatedArtifactInspectionRepository {

}
