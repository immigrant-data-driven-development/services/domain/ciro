package br.nemo.immigrant.ontology.service.ciro.continuousbuild.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.SourceCodeCopy;
import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.repositories.SourceCodeCopyRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "sourcecodecopy", path = "sourcecodecopy")
public interface SourceCodeCopyRepositoryWeb extends SourceCodeCopyRepository {

}
