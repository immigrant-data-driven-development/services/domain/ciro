package br.nemo.immigrant.ontology.service.ciro.continuoustest.controllers;

import br.nemo.immigrant.ontology.entity.ciro.continuoustest.models.AutomatedTesting;
import br.nemo.immigrant.ontology.entity.ciro.continuoustest.repositories.AutomatedTestingRepository;
import br.nemo.immigrant.ontology.service.ciro.continuoustest.records.AutomatedTestingInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class AutomatedTestingController  {

  @Autowired
  AutomatedTestingRepository repository;

  @QueryMapping
  public List<AutomatedTesting> findAllAutomatedTestings() {
    return repository.findAll();
  }

  @QueryMapping
  public AutomatedTesting findByIDAutomatedTesting(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public AutomatedTesting createAutomatedTesting(@Argument AutomatedTestingInput input) {
    AutomatedTesting instance = AutomatedTesting.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public AutomatedTesting updateAutomatedTesting(@Argument Long id, @Argument AutomatedTestingInput input) {
    AutomatedTesting instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("AutomatedTesting not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteAutomatedTesting(@Argument Long id) {
    repository.deleteById(id);
  }

}
