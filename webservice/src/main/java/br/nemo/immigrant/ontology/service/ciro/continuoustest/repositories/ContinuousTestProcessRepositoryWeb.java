package br.nemo.immigrant.ontology.service.ciro.continuoustest.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuoustest.models.ContinuousTestProcess;
import br.nemo.immigrant.ontology.entity.ciro.continuoustest.repositories.ContinuousTestProcessRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "continuoustestprocess", path = "continuoustestprocess")
public interface ContinuousTestProcessRepositoryWeb extends ContinuousTestProcessRepository {

}
