package br.nemo.immigrant.ontology.service.ciro.continuousinspection.controllers;

import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.models.QualityAssuranceCriterionCode;
import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.repositories.QualityAssuranceCriterionCodeRepository;
import br.nemo.immigrant.ontology.service.ciro.continuousinspection.records.QualityAssuranceCriterionCodeInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class QualityAssuranceCriterionCodeController  {

  @Autowired
  QualityAssuranceCriterionCodeRepository repository;

  @QueryMapping
  public List<QualityAssuranceCriterionCode> findAllQualityAssuranceCriterionCodes() {
    return repository.findAll();
  }

  @QueryMapping
  public QualityAssuranceCriterionCode findByIDQualityAssuranceCriterionCode(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public QualityAssuranceCriterionCode createQualityAssuranceCriterionCode(@Argument QualityAssuranceCriterionCodeInput input) {
    QualityAssuranceCriterionCode instance = QualityAssuranceCriterionCode.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public QualityAssuranceCriterionCode updateQualityAssuranceCriterionCode(@Argument Long id, @Argument QualityAssuranceCriterionCodeInput input) {
    QualityAssuranceCriterionCode instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("QualityAssuranceCriterionCode not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteQualityAssuranceCriterionCode(@Argument Long id) {
    repository.deleteById(id);
  }

}
