package br.nemo.immigrant.ontology.service.ciro.continuousbuild.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.ContinuousBuildProcess;
import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.repositories.ContinuousBuildProcessRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "continuousbuildprocess", path = "continuousbuildprocess")
public interface ContinuousBuildProcessRepositoryWeb extends ContinuousBuildProcessRepository {

}
