package br.nemo.immigrant.ontology.service.ciro.continuoustest.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuoustest.models.AutomatedTestExecution;
import br.nemo.immigrant.ontology.entity.ciro.continuoustest.repositories.AutomatedTestExecutionRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "automatedtestexecution", path = "automatedtestexecution")
public interface AutomatedTestExecutionRepositoryWeb extends AutomatedTestExecutionRepository {

}
