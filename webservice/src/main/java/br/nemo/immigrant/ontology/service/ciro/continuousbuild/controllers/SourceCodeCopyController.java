package br.nemo.immigrant.ontology.service.ciro.continuousbuild.controllers;

import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.SourceCodeCopy;
import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.repositories.SourceCodeCopyRepository;
import br.nemo.immigrant.ontology.service.ciro.continuousbuild.records.SourceCodeCopyInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class SourceCodeCopyController  {

  @Autowired
  SourceCodeCopyRepository repository;

  @QueryMapping
  public List<SourceCodeCopy> findAllSourceCodeCopys() {
    return repository.findAll();
  }

  @QueryMapping
  public SourceCodeCopy findByIDSourceCodeCopy(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public SourceCodeCopy createSourceCodeCopy(@Argument SourceCodeCopyInput input) {
    SourceCodeCopy instance = SourceCodeCopy.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public SourceCodeCopy updateSourceCodeCopy(@Argument Long id, @Argument SourceCodeCopyInput input) {
    SourceCodeCopy instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("SourceCodeCopy not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteSourceCodeCopy(@Argument Long id) {
    repository.deleteById(id);
  }

}
