package br.nemo.immigrant.ontology.service.ciro.continuousbuild.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.CodeCheckout;
import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.repositories.CodeCheckoutRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "codecheckout", path = "codecheckout")
public interface CodeCheckoutRepositoryWeb extends CodeCheckoutRepository {

}
