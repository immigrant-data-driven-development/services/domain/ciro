package br.nemo.immigrant.ontology.service.ciro.continuousintegration.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousintegration.models.ContinuousFeedbackActivity;
import br.nemo.immigrant.ontology.entity.ciro.continuousintegration.repositories.ContinuousFeedbackActivityRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "continuousfeedbackactivity", path = "continuousfeedbackactivity")
public interface ContinuousFeedbackActivityRepositoryWeb extends ContinuousFeedbackActivityRepository {

}
