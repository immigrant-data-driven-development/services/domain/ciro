package br.nemo.immigrant.ontology.service.ciro.continuousinspection.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.models.QualityAssuranceCriterionCode;
import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.repositories.QualityAssuranceCriterionCodeRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "qualityassurancecriterioncode", path = "qualityassurancecriterioncode")
public interface QualityAssuranceCriterionCodeRepositoryWeb extends QualityAssuranceCriterionCodeRepository {

}
