package br.nemo.immigrant.ontology.service.ciro.continuousbuild.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.BuildEnviromentCreation;
import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.repositories.BuildEnviromentCreationRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "buildenviromentcreation", path = "buildenviromentcreation")
public interface BuildEnviromentCreationRepositoryWeb extends BuildEnviromentCreationRepository {

}
