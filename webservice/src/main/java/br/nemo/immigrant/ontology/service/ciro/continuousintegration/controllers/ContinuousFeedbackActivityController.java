package br.nemo.immigrant.ontology.service.ciro.continuousintegration.controllers;

import br.nemo.immigrant.ontology.entity.ciro.continuousintegration.models.ContinuousFeedbackActivity;
import br.nemo.immigrant.ontology.entity.ciro.continuousintegration.repositories.ContinuousFeedbackActivityRepository;
import br.nemo.immigrant.ontology.service.ciro.continuousintegration.records.ContinuousFeedbackActivityInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ContinuousFeedbackActivityController  {

  @Autowired
  ContinuousFeedbackActivityRepository repository;

  @QueryMapping
  public List<ContinuousFeedbackActivity> findAllContinuousFeedbackActivitys() {
    return repository.findAll();
  }

  @QueryMapping
  public ContinuousFeedbackActivity findByIDContinuousFeedbackActivity(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public ContinuousFeedbackActivity createContinuousFeedbackActivity(@Argument ContinuousFeedbackActivityInput input) {
    ContinuousFeedbackActivity instance = ContinuousFeedbackActivity.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public ContinuousFeedbackActivity updateContinuousFeedbackActivity(@Argument Long id, @Argument ContinuousFeedbackActivityInput input) {
    ContinuousFeedbackActivity instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("ContinuousFeedbackActivity not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteContinuousFeedbackActivity(@Argument Long id) {
    repository.deleteById(id);
  }

}
