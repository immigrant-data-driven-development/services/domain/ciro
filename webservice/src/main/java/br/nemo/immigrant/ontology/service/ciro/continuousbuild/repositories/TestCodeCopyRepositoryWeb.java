package br.nemo.immigrant.ontology.service.ciro.continuousbuild.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.TestCodeCopy;
import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.repositories.TestCodeCopyRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "testcodecopy", path = "testcodecopy")
public interface TestCodeCopyRepositoryWeb extends TestCodeCopyRepository {

}
