package br.nemo.immigrant.ontology.service.ciro.continuousinspection.controllers;

import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.models.AutomatedAdherenceInspection;
import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.repositories.AutomatedAdherenceInspectionRepository;
import br.nemo.immigrant.ontology.service.ciro.continuousinspection.records.AutomatedAdherenceInspectionInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class AutomatedAdherenceInspectionController  {

  @Autowired
  AutomatedAdherenceInspectionRepository repository;

  @QueryMapping
  public List<AutomatedAdherenceInspection> findAllAutomatedAdherenceInspections() {
    return repository.findAll();
  }

  @QueryMapping
  public AutomatedAdherenceInspection findByIDAutomatedAdherenceInspection(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public AutomatedAdherenceInspection createAutomatedAdherenceInspection(@Argument AutomatedAdherenceInspectionInput input) {
    AutomatedAdherenceInspection instance = AutomatedAdherenceInspection.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public AutomatedAdherenceInspection updateAutomatedAdherenceInspection(@Argument Long id, @Argument AutomatedAdherenceInspectionInput input) {
    AutomatedAdherenceInspection instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("AutomatedAdherenceInspection not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteAutomatedAdherenceInspection(@Argument Long id) {
    repository.deleteById(id);
  }

}
