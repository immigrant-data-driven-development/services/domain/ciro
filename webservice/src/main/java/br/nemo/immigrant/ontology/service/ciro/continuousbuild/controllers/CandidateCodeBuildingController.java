package br.nemo.immigrant.ontology.service.ciro.continuousbuild.controllers;

import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.CandidateCodeBuilding;
import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.repositories.CandidateCodeBuildingRepository;
import br.nemo.immigrant.ontology.service.ciro.continuousbuild.records.CandidateCodeBuildingInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class CandidateCodeBuildingController  {

  @Autowired
  CandidateCodeBuildingRepository repository;

  @QueryMapping
  public List<CandidateCodeBuilding> findAllCandidateCodeBuildings() {
    return repository.findAll();
  }

  @QueryMapping
  public CandidateCodeBuilding findByIDCandidateCodeBuilding(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public CandidateCodeBuilding createCandidateCodeBuilding(@Argument CandidateCodeBuildingInput input) {
    CandidateCodeBuilding instance = CandidateCodeBuilding.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public CandidateCodeBuilding updateCandidateCodeBuilding(@Argument Long id, @Argument CandidateCodeBuildingInput input) {
    CandidateCodeBuilding instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("CandidateCodeBuilding not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteCandidateCodeBuilding(@Argument Long id) {
    repository.deleteById(id);
  }

}
