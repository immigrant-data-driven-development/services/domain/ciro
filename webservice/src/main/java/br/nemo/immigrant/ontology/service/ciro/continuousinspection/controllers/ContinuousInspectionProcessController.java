package br.nemo.immigrant.ontology.service.ciro.continuousinspection.controllers;

import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.models.ContinuousInspectionProcess;
import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.repositories.ContinuousInspectionProcessRepository;
import br.nemo.immigrant.ontology.service.ciro.continuousinspection.records.ContinuousInspectionProcessInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ContinuousInspectionProcessController  {

  @Autowired
  ContinuousInspectionProcessRepository repository;

  @QueryMapping
  public List<ContinuousInspectionProcess> findAllContinuousInspectionProcesss() {
    return repository.findAll();
  }

  @QueryMapping
  public ContinuousInspectionProcess findByIDContinuousInspectionProcess(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public ContinuousInspectionProcess createContinuousInspectionProcess(@Argument ContinuousInspectionProcessInput input) {
    ContinuousInspectionProcess instance = ContinuousInspectionProcess.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public ContinuousInspectionProcess updateContinuousInspectionProcess(@Argument Long id, @Argument ContinuousInspectionProcessInput input) {
    ContinuousInspectionProcess instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("ContinuousInspectionProcess not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteContinuousInspectionProcess(@Argument Long id) {
    repository.deleteById(id);
  }

}
