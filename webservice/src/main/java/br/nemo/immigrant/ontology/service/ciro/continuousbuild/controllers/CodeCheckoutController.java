package br.nemo.immigrant.ontology.service.ciro.continuousbuild.controllers;

import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.CodeCheckout;
import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.repositories.CodeCheckoutRepository;
import br.nemo.immigrant.ontology.service.ciro.continuousbuild.records.CodeCheckoutInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class CodeCheckoutController  {

  @Autowired
  CodeCheckoutRepository repository;

  @QueryMapping
  public List<CodeCheckout> findAllCodeCheckouts() {
    return repository.findAll();
  }

  @QueryMapping
  public CodeCheckout findByIDCodeCheckout(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public CodeCheckout createCodeCheckout(@Argument CodeCheckoutInput input) {
    CodeCheckout instance = CodeCheckout.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public CodeCheckout updateCodeCheckout(@Argument Long id, @Argument CodeCheckoutInput input) {
    CodeCheckout instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("CodeCheckout not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteCodeCheckout(@Argument Long id) {
    repository.deleteById(id);
  }

}
