package br.nemo.immigrant.ontology.service.ciro.continuousintegration.controllers;

import br.nemo.immigrant.ontology.entity.ciro.continuousintegration.models.ContinuousIntegrationServer;
import br.nemo.immigrant.ontology.entity.ciro.continuousintegration.repositories.ContinuousIntegrationServerRepository;
import br.nemo.immigrant.ontology.service.ciro.continuousintegration.records.ContinuousIntegrationServerInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ContinuousIntegrationServerController  {

  @Autowired
  ContinuousIntegrationServerRepository repository;

  @QueryMapping
  public List<ContinuousIntegrationServer> findAllContinuousIntegrationServers() {
    return repository.findAll();
  }

  @QueryMapping
  public ContinuousIntegrationServer findByIDContinuousIntegrationServer(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public ContinuousIntegrationServer createContinuousIntegrationServer(@Argument ContinuousIntegrationServerInput input) {
    ContinuousIntegrationServer instance = ContinuousIntegrationServer.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public ContinuousIntegrationServer updateContinuousIntegrationServer(@Argument Long id, @Argument ContinuousIntegrationServerInput input) {
    ContinuousIntegrationServer instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("ContinuousIntegrationServer not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteContinuousIntegrationServer(@Argument Long id) {
    repository.deleteById(id);
  }

}
