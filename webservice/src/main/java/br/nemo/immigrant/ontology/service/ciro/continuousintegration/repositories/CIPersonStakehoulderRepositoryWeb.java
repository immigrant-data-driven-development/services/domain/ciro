package br.nemo.immigrant.ontology.service.ciro.continuousintegration.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousintegration.models.CIPersonStakehoulder;
import br.nemo.immigrant.ontology.entity.ciro.continuousintegration.repositories.CIPersonStakehoulderRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "cipersonstakehoulder", path = "cipersonstakehoulder")
public interface CIPersonStakehoulderRepositoryWeb extends CIPersonStakehoulderRepository {

}
