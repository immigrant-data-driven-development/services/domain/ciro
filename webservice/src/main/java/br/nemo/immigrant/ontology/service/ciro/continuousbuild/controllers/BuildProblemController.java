package br.nemo.immigrant.ontology.service.ciro.continuousbuild.controllers;

import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.BuildProblem;
import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.repositories.BuildProblemRepository;
import br.nemo.immigrant.ontology.service.ciro.continuousbuild.records.BuildProblemInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class BuildProblemController  {

  @Autowired
  BuildProblemRepository repository;

  @QueryMapping
  public List<BuildProblem> findAllBuildProblems() {
    return repository.findAll();
  }

  @QueryMapping
  public BuildProblem findByIDBuildProblem(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public BuildProblem createBuildProblem(@Argument BuildProblemInput input) {
    BuildProblem instance = BuildProblem.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public BuildProblem updateBuildProblem(@Argument Long id, @Argument BuildProblemInput input) {
    BuildProblem instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("BuildProblem not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteBuildProblem(@Argument Long id) {
    repository.deleteById(id);
  }

}
