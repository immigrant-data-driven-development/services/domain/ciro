package br.nemo.immigrant.ontology.service.ciro.continuousintegration.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousintegration.models.ContinuousIntegrationProcess;
import br.nemo.immigrant.ontology.entity.ciro.continuousintegration.repositories.ContinuousIntegrationProcessRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "continuousintegrationprocess", path = "continuousintegrationprocess")
public interface ContinuousIntegrationProcessRepositoryWeb extends ContinuousIntegrationProcessRepository {

}
