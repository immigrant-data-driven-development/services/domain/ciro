package br.nemo.immigrant.ontology.service.ciro.continuousintegration.controllers;

import br.nemo.immigrant.ontology.entity.ciro.continuousintegration.models.CITeamStakehoulder;
import br.nemo.immigrant.ontology.entity.ciro.continuousintegration.repositories.CITeamStakehoulderRepository;
import br.nemo.immigrant.ontology.service.ciro.continuousintegration.records.CITeamStakehoulderInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class CITeamStakehoulderController  {

  @Autowired
  CITeamStakehoulderRepository repository;

  @QueryMapping
  public List<CITeamStakehoulder> findAllCITeamStakehoulders() {
    return repository.findAll();
  }

  @QueryMapping
  public CITeamStakehoulder findByIDCITeamStakehoulder(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public CITeamStakehoulder createCITeamStakehoulder(@Argument CITeamStakehoulderInput input) {
    CITeamStakehoulder instance = CITeamStakehoulder.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public CITeamStakehoulder updateCITeamStakehoulder(@Argument Long id, @Argument CITeamStakehoulderInput input) {
    CITeamStakehoulder instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("CITeamStakehoulder not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteCITeamStakehoulder(@Argument Long id) {
    repository.deleteById(id);
  }

}
