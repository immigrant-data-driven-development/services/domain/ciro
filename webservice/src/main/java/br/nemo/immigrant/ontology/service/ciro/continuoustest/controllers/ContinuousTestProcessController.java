package br.nemo.immigrant.ontology.service.ciro.continuoustest.controllers;

import br.nemo.immigrant.ontology.entity.ciro.continuoustest.models.ContinuousTestProcess;
import br.nemo.immigrant.ontology.entity.ciro.continuoustest.repositories.ContinuousTestProcessRepository;
import br.nemo.immigrant.ontology.service.ciro.continuoustest.records.ContinuousTestProcessInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ContinuousTestProcessController  {

  @Autowired
  ContinuousTestProcessRepository repository;

  @QueryMapping
  public List<ContinuousTestProcess> findAllContinuousTestProcesss() {
    return repository.findAll();
  }

  @QueryMapping
  public ContinuousTestProcess findByIDContinuousTestProcess(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public ContinuousTestProcess createContinuousTestProcess(@Argument ContinuousTestProcessInput input) {
    ContinuousTestProcess instance = ContinuousTestProcess.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public ContinuousTestProcess updateContinuousTestProcess(@Argument Long id, @Argument ContinuousTestProcessInput input) {
    ContinuousTestProcess instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("ContinuousTestProcess not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteContinuousTestProcess(@Argument Long id) {
    repository.deleteById(id);
  }

}
