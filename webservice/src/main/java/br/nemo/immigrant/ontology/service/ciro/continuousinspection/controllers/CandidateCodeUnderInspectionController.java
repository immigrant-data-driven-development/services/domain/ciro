package br.nemo.immigrant.ontology.service.ciro.continuousinspection.controllers;

import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.models.CandidateCodeUnderInspection;
import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.repositories.CandidateCodeUnderInspectionRepository;
import br.nemo.immigrant.ontology.service.ciro.continuousinspection.records.CandidateCodeUnderInspectionInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class CandidateCodeUnderInspectionController  {

  @Autowired
  CandidateCodeUnderInspectionRepository repository;

  @QueryMapping
  public List<CandidateCodeUnderInspection> findAllCandidateCodeUnderInspections() {
    return repository.findAll();
  }

  @QueryMapping
  public CandidateCodeUnderInspection findByIDCandidateCodeUnderInspection(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public CandidateCodeUnderInspection createCandidateCodeUnderInspection(@Argument CandidateCodeUnderInspectionInput input) {
    CandidateCodeUnderInspection instance = CandidateCodeUnderInspection.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public CandidateCodeUnderInspection updateCandidateCodeUnderInspection(@Argument Long id, @Argument CandidateCodeUnderInspectionInput input) {
    CandidateCodeUnderInspection instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("CandidateCodeUnderInspection not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteCandidateCodeUnderInspection(@Argument Long id) {
    repository.deleteById(id);
  }

}
