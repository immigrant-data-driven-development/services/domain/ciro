package br.nemo.immigrant.ontology.service.ciro.continuousbuild.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.CandidateCode;
import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.repositories.CandidateCodeRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "candidatecode", path = "candidatecode")
public interface CandidateCodeRepositoryWeb extends CandidateCodeRepository {

}
