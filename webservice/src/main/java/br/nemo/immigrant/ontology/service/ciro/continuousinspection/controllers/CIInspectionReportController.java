package br.nemo.immigrant.ontology.service.ciro.continuousinspection.controllers;

import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.models.CIInspectionReport;
import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.repositories.CIInspectionReportRepository;
import br.nemo.immigrant.ontology.service.ciro.continuousinspection.records.CIInspectionReportInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class CIInspectionReportController  {

  @Autowired
  CIInspectionReportRepository repository;

  @QueryMapping
  public List<CIInspectionReport> findAllCIInspectionReports() {
    return repository.findAll();
  }

  @QueryMapping
  public CIInspectionReport findByIDCIInspectionReport(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public CIInspectionReport createCIInspectionReport(@Argument CIInspectionReportInput input) {
    CIInspectionReport instance = CIInspectionReport.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public CIInspectionReport updateCIInspectionReport(@Argument Long id, @Argument CIInspectionReportInput input) {
    CIInspectionReport instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("CIInspectionReport not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteCIInspectionReport(@Argument Long id) {
    repository.deleteById(id);
  }

}
