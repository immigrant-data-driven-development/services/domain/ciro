package br.nemo.immigrant.ontology.service.ciro.continuousbuild.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.BuildProblem;
import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.repositories.BuildProblemRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "buildproblem", path = "buildproblem")
public interface BuildProblemRepositoryWeb extends BuildProblemRepository {

}
