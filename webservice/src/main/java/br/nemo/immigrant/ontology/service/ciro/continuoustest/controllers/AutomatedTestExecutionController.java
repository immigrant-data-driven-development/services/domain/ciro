package br.nemo.immigrant.ontology.service.ciro.continuoustest.controllers;

import br.nemo.immigrant.ontology.entity.ciro.continuoustest.models.AutomatedTestExecution;
import br.nemo.immigrant.ontology.entity.ciro.continuoustest.repositories.AutomatedTestExecutionRepository;
import br.nemo.immigrant.ontology.service.ciro.continuoustest.records.AutomatedTestExecutionInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class AutomatedTestExecutionController  {

  @Autowired
  AutomatedTestExecutionRepository repository;

  @QueryMapping
  public List<AutomatedTestExecution> findAllAutomatedTestExecutions() {
    return repository.findAll();
  }

  @QueryMapping
  public AutomatedTestExecution findByIDAutomatedTestExecution(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public AutomatedTestExecution createAutomatedTestExecution(@Argument AutomatedTestExecutionInput input) {
    AutomatedTestExecution instance = AutomatedTestExecution.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public AutomatedTestExecution updateAutomatedTestExecution(@Argument Long id, @Argument AutomatedTestExecutionInput input) {
    AutomatedTestExecution instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("AutomatedTestExecution not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteAutomatedTestExecution(@Argument Long id) {
    repository.deleteById(id);
  }

}
