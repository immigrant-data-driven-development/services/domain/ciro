package br.nemo.immigrant.ontology.service.ciro.continuousbuild.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.CandidateCodeBuilding;
import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.repositories.CandidateCodeBuildingRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "candidatecodebuilding", path = "candidatecodebuilding")
public interface CandidateCodeBuildingRepositoryWeb extends CandidateCodeBuildingRepository {

}
