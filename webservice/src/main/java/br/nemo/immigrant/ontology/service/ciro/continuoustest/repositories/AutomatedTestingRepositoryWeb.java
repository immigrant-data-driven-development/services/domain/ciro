package br.nemo.immigrant.ontology.service.ciro.continuoustest.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuoustest.models.AutomatedTesting;
import br.nemo.immigrant.ontology.entity.ciro.continuoustest.repositories.AutomatedTestingRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "automatedtesting", path = "automatedtesting")
public interface AutomatedTestingRepositoryWeb extends AutomatedTestingRepository {

}
