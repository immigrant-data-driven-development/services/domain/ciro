package br.nemo.immigrant.ontology.service.ciro.continuoustest.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuoustest.models.CITestingEnvironmentCreation;
import br.nemo.immigrant.ontology.entity.ciro.continuoustest.repositories.CITestingEnvironmentCreationRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "citestingenvironmentcreation", path = "citestingenvironmentcreation")
public interface CITestingEnvironmentCreationRepositoryWeb extends CITestingEnvironmentCreationRepository {

}
