package br.nemo.immigrant.ontology.service.ciro.continuoustest.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuoustest.models.CITestResult;
import br.nemo.immigrant.ontology.entity.ciro.continuoustest.repositories.CITestResultRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "citestresult", path = "citestresult")
public interface CITestResultRepositoryWeb extends CITestResultRepository {

}
