package br.nemo.immigrant.ontology.service.ciro.continuousinspection.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.models.CIInspectionReport;
import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.repositories.CIInspectionReportRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "ciinspectionreport", path = "ciinspectionreport")
public interface CIInspectionReportRepositoryWeb extends CIInspectionReportRepository {

}
