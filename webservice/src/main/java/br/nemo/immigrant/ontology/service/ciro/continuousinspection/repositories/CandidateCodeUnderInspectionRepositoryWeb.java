package br.nemo.immigrant.ontology.service.ciro.continuousinspection.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.models.CandidateCodeUnderInspection;
import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.repositories.CandidateCodeUnderInspectionRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "candidatecodeunderinspection", path = "candidatecodeunderinspection")
public interface CandidateCodeUnderInspectionRepositoryWeb extends CandidateCodeUnderInspectionRepository {

}
