package br.nemo.immigrant.ontology.service.ciro.continuousinspection.controllers;

import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.models.CIInspectionEnvironmentCreation;
import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.repositories.CIInspectionEnvironmentCreationRepository;
import br.nemo.immigrant.ontology.service.ciro.continuousinspection.records.CIInspectionEnvironmentCreationInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class CIInspectionEnvironmentCreationController  {

  @Autowired
  CIInspectionEnvironmentCreationRepository repository;

  @QueryMapping
  public List<CIInspectionEnvironmentCreation> findAllCIInspectionEnvironmentCreations() {
    return repository.findAll();
  }

  @QueryMapping
  public CIInspectionEnvironmentCreation findByIDCIInspectionEnvironmentCreation(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public CIInspectionEnvironmentCreation createCIInspectionEnvironmentCreation(@Argument CIInspectionEnvironmentCreationInput input) {
    CIInspectionEnvironmentCreation instance = CIInspectionEnvironmentCreation.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public CIInspectionEnvironmentCreation updateCIInspectionEnvironmentCreation(@Argument Long id, @Argument CIInspectionEnvironmentCreationInput input) {
    CIInspectionEnvironmentCreation instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("CIInspectionEnvironmentCreation not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteCIInspectionEnvironmentCreation(@Argument Long id) {
    repository.deleteById(id);
  }

}
