package br.nemo.immigrant.ontology.service.ciro.continuousintegration.controllers;

import br.nemo.immigrant.ontology.entity.ciro.continuousintegration.models.CIPersonStakehoulder;
import br.nemo.immigrant.ontology.entity.ciro.continuousintegration.repositories.CIPersonStakehoulderRepository;
import br.nemo.immigrant.ontology.service.ciro.continuousintegration.records.CIPersonStakehoulderInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class CIPersonStakehoulderController  {

  @Autowired
  CIPersonStakehoulderRepository repository;

  @QueryMapping
  public List<CIPersonStakehoulder> findAllCIPersonStakehoulders() {
    return repository.findAll();
  }

  @QueryMapping
  public CIPersonStakehoulder findByIDCIPersonStakehoulder(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public CIPersonStakehoulder createCIPersonStakehoulder(@Argument CIPersonStakehoulderInput input) {
    CIPersonStakehoulder instance = CIPersonStakehoulder.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public CIPersonStakehoulder updateCIPersonStakehoulder(@Argument Long id, @Argument CIPersonStakehoulderInput input) {
    CIPersonStakehoulder instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("CIPersonStakehoulder not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteCIPersonStakehoulder(@Argument Long id) {
    repository.deleteById(id);
  }

}
