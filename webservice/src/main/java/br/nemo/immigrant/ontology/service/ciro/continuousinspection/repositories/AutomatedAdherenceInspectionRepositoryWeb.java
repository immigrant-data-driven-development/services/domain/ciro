package br.nemo.immigrant.ontology.service.ciro.continuousinspection.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.models.AutomatedAdherenceInspection;
import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.repositories.AutomatedAdherenceInspectionRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "automatedadherenceinspection", path = "automatedadherenceinspection")
public interface AutomatedAdherenceInspectionRepositoryWeb extends AutomatedAdherenceInspectionRepository {

}
