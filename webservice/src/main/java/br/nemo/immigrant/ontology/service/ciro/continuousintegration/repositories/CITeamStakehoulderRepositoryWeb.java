package br.nemo.immigrant.ontology.service.ciro.continuousintegration.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousintegration.models.CITeamStakehoulder;
import br.nemo.immigrant.ontology.entity.ciro.continuousintegration.repositories.CITeamStakehoulderRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "citeamstakehoulder", path = "citeamstakehoulder")
public interface CITeamStakehoulderRepositoryWeb extends CITeamStakehoulderRepository {

}
