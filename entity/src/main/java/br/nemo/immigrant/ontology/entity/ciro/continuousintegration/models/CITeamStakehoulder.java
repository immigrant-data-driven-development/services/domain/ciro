package br.nemo.immigrant.ontology.entity.ciro.continuousintegration.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;



import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectTeamStakeholder;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "citeamstakehoulder")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class CITeamStakehoulder extends ProjectTeamStakeholder implements Serializable {


  @ManyToMany
  @JoinTable(
      name = "citeamstakehoulder_continuousintegrationprocess",
      joinColumns = @JoinColumn(name = "citeamstakehoulder_id"),
      inverseJoinColumns = @JoinColumn(name = "continuousintegrationprocess_id")
  )
  @Builder.Default
  private Set<ContinuousIntegrationProcess> continuousintegrationprocesss = new HashSet<>();

  @ManyToMany
  @JoinTable(
      name = "citeamstakehoulder_continuousfeedbackactivity",
      joinColumns = @JoinColumn(name = "citeamstakehoulder_id"),
      inverseJoinColumns = @JoinColumn(name = "continuousfeedbackactivity_id")
  )
  @Builder.Default
  private Set<ContinuousFeedbackActivity> continuousfeedbackactivitys = new HashSet<>();

  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        CITeamStakehoulder elem = (CITeamStakehoulder) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "CITeamStakehoulder {" +
         "id="+this.id+


      '}';
  }
}
