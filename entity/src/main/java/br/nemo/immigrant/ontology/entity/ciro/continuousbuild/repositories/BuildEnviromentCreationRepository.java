package br.nemo.immigrant.ontology.entity.ciro.continuousbuild.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.BuildEnviromentCreation;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface BuildEnviromentCreationRepository extends PagingAndSortingRepository<BuildEnviromentCreation, Long>, ListCrudRepository<BuildEnviromentCreation, Long> {

}
