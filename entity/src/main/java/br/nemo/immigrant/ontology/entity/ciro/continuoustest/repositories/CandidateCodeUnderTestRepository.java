package br.nemo.immigrant.ontology.entity.ciro.continuoustest.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuoustest.models.CandidateCodeUnderTest;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface CandidateCodeUnderTestRepository extends PagingAndSortingRepository<CandidateCodeUnderTest, Long>, ListCrudRepository<CandidateCodeUnderTest, Long> {

}
