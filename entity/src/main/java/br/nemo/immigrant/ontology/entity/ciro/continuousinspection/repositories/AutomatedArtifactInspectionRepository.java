package br.nemo.immigrant.ontology.entity.ciro.continuousinspection.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.models.AutomatedArtifactInspection;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface AutomatedArtifactInspectionRepository extends PagingAndSortingRepository<AutomatedArtifactInspection, Long>, ListCrudRepository<AutomatedArtifactInspection, Long> {

}
