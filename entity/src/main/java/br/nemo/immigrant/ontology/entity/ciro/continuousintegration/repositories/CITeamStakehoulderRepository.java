package br.nemo.immigrant.ontology.entity.ciro.continuousintegration.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousintegration.models.CITeamStakehoulder;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface CITeamStakehoulderRepository extends PagingAndSortingRepository<CITeamStakehoulder, Long>, ListCrudRepository<CITeamStakehoulder, Long> {

}
