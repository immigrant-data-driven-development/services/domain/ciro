package br.nemo.immigrant.ontology.entity.ciro.continuoustest.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;

/* 
import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.SourceCodeCopy;
import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.TestCodeCopy;
import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.CandidateCodeBuilding;
*/
import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.CandidateCode;

import br.nemo.immigrant.ontology.entity.spo.artifact.models.Artifact;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "candidatecodeundertest")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class CandidateCodeUnderTest extends CandidateCode implements Serializable {


  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        CandidateCodeUnderTest elem = (CandidateCodeUnderTest) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "CandidateCodeUnderTest {" +
         "id="+this.id+



      '}';
  }
}
