package br.nemo.immigrant.ontology.entity.ciro.continuousbuild.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.CandidateCode;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface CandidateCodeRepository extends PagingAndSortingRepository<CandidateCode, Long>, ListCrudRepository<CandidateCode, Long> {

}
