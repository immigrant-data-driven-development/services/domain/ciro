package br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;

import br.nemo.immigrant.ontology.entity.ciro.continuousintegration.models.ContinuousIntegrationServer;
import br.nemo.immigrant.ontology.entity.ciro.continuousintegration.models.ContinuousIntegrationProcess;

import br.nemo.immigrant.ontology.entity.spo.process.models.SpecificProjectProcess;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "continuousbuildprocess")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class ContinuousBuildProcess extends SpecificProjectProcess implements Serializable {




  @ManyToMany(mappedBy = "continuousbuildprocesss")
  @Builder.Default
  private Set<ContinuousIntegrationServer> ContinuousIntegrationServers = new HashSet<>();
  
  @Builder.Default
  @Enumerated(EnumType.STRING)
  private SuccessfulType successfultype = SuccessfulType.SUCESSFUL;


  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        ContinuousBuildProcess elem = (ContinuousBuildProcess) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "ContinuousBuildProcess {" +
         "id="+this.id+

          ", successfultype='"+this.successfultype+"'"+
      '}';
  }
}
