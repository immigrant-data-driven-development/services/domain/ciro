package br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models;

public enum Integrated {
    CODEINTEGRATED,
    CODEUNDEINTEGRADED
}