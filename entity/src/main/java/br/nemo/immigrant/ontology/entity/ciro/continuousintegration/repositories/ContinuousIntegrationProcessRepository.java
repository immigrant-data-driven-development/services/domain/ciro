package br.nemo.immigrant.ontology.entity.ciro.continuousintegration.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousintegration.models.ContinuousIntegrationProcess;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface ContinuousIntegrationProcessRepository extends PagingAndSortingRepository<ContinuousIntegrationProcess, Long>, ListCrudRepository<ContinuousIntegrationProcess, Long> {

}
