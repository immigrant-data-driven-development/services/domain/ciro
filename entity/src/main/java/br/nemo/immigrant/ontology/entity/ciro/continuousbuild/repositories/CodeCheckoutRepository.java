package br.nemo.immigrant.ontology.entity.ciro.continuousbuild.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.CodeCheckout;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface CodeCheckoutRepository extends PagingAndSortingRepository<CodeCheckout, Long>, ListCrudRepository<CodeCheckout, Long> {

}
