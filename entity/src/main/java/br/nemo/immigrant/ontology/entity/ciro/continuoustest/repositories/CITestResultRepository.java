package br.nemo.immigrant.ontology.entity.ciro.continuoustest.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuoustest.models.CITestResult;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface CITestResultRepository extends PagingAndSortingRepository<CITestResult, Long>, ListCrudRepository<CITestResult, Long> {

}
