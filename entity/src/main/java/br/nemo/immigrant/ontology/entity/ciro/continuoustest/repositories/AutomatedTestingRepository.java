package br.nemo.immigrant.ontology.entity.ciro.continuoustest.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuoustest.models.AutomatedTesting;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface AutomatedTestingRepository extends PagingAndSortingRepository<AutomatedTesting, Long>, ListCrudRepository<AutomatedTesting, Long> {

}
