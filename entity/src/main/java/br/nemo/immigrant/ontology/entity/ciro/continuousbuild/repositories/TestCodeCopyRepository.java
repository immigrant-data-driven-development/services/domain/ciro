package br.nemo.immigrant.ontology.entity.ciro.continuousbuild.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.TestCodeCopy;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface TestCodeCopyRepository extends PagingAndSortingRepository<TestCodeCopy, Long>, ListCrudRepository<TestCodeCopy, Long> {

}
