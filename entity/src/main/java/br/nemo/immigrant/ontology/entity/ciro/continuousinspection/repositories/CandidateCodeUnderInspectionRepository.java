package br.nemo.immigrant.ontology.entity.ciro.continuousinspection.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.models.CandidateCodeUnderInspection;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface CandidateCodeUnderInspectionRepository extends PagingAndSortingRepository<CandidateCodeUnderInspection, Long>, ListCrudRepository<CandidateCodeUnderInspection, Long> {

}
