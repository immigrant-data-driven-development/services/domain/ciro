package br.nemo.immigrant.ontology.entity.ciro.continuousbuild.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.CandidateCodeBuilding;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface CandidateCodeBuildingRepository extends PagingAndSortingRepository<CandidateCodeBuilding, Long>, ListCrudRepository<CandidateCodeBuilding, Long> {

}
