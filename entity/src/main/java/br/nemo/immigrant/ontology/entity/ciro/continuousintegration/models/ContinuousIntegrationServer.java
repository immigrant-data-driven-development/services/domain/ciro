package br.nemo.immigrant.ontology.entity.ciro.continuousintegration.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;

import br.nemo.immigrant.ontology.entity.sysswo.system.models.LoadedSoftwareSystemCopy;
import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.ContinuousBuildProcess;
import br.nemo.immigrant.ontology.entity.ciro.continuoustest.models.ContinuousTestProcess;
import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.models.ContinuousInspectionProcess;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "continuousintegratioserver")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class ContinuousIntegrationServer extends LoadedSoftwareSystemCopy implements Serializable {

  @ManyToMany
  @JoinTable(
      name = "continuousintegrationserver_continuousintegrationprocess",
      joinColumns = @JoinColumn(name = "continuousintegrationserver_id"),
      inverseJoinColumns = @JoinColumn(name = "continuousintegrationprocess_id")
  )
  @Builder.Default
  private Set<ContinuousIntegrationProcess> continuousintegrationprocesss = new HashSet<>();

  @ManyToMany
  @JoinTable(
      name = "continuousintegrationserver_continuousbuildprocess",
      joinColumns = @JoinColumn(name = "continuousintegrationserver_id"),
      inverseJoinColumns = @JoinColumn(name = "continuousbuildprocess_id")
  )
  @Builder.Default
  private Set<ContinuousBuildProcess> continuousbuildprocesss = new HashSet<>();

  @ManyToMany
  @JoinTable(
      name = "continuousintegrationserver_continuoustestprocess",
      joinColumns = @JoinColumn(name = "continuousintegrationserver_id"),
      inverseJoinColumns = @JoinColumn(name = "continuoustestprocess_id")
  )
  @Builder.Default
  private Set<ContinuousTestProcess> continuoustestprocesss = new HashSet<>();

  @ManyToMany
  @JoinTable(
      name = "continuousintegrationserver_continuousinspectionprocess",
      joinColumns = @JoinColumn(name = "continuousintegrationserver_id"),
      inverseJoinColumns = @JoinColumn(name = "continuousinspectionprocess_id")
  )
  @Builder.Default
  private Set<ContinuousInspectionProcess> continuousinspectionprocesss = new HashSet<>();
  
  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        ContinuousIntegrationServer elem = (ContinuousIntegrationServer) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "ContinuousIntegratioServer {" +
         "id="+this.id+


      '}';
  }
}
