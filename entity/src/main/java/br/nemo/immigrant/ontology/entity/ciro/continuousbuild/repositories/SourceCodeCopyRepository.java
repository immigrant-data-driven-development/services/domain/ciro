package br.nemo.immigrant.ontology.entity.ciro.continuousbuild.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.SourceCodeCopy;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface SourceCodeCopyRepository extends PagingAndSortingRepository<SourceCodeCopy, Long>, ListCrudRepository<SourceCodeCopy, Long> {

}
