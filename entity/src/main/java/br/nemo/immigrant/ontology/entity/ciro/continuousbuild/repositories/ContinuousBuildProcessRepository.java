package br.nemo.immigrant.ontology.entity.ciro.continuousbuild.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.ContinuousBuildProcess;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface ContinuousBuildProcessRepository extends PagingAndSortingRepository<ContinuousBuildProcess, Long>, ListCrudRepository<ContinuousBuildProcess, Long> {

}
