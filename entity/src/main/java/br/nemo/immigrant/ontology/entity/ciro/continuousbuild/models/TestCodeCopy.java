package br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;



import br.nemo.immigrant.ontology.entity.roost.testingprocess.models.TestCode;
//import br.nemo.immigrant.ontology.entity.spo.artifact.models.Artifact;
@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "testcodecopy")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class TestCodeCopy extends TestCode implements Serializable {


/*

  @ManyToMany(mappedBy = "testcodecopys")
  @Builder.Default
  private Set<CodeCheckout> codecheckouts = new HashSet<>();

  @ManyToMany(mappedBy = "testcodecopys")
  @Builder.Default
  private Set<CandidateCode> candidatecodes = new HashSet<>();
*/

  @Builder.Default
  @Enumerated(EnumType.STRING)
  private Integrated codetype = Integrated.CODEINTEGRATED;


  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        TestCodeCopy elem = (TestCodeCopy) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "TestCodeCopy {" +
         "id="+this.id+

          ", codetype='"+this.codetype+"'"+
      '}';
  }
}
