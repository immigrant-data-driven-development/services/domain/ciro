package br.nemo.immigrant.ontology.entity.ciro.continuousintegration.models;

public enum ContinuousIntegrationProcessType {
    CHECKINCIPROCESS,
    SCHEDULED,
    ONDEMAND
}