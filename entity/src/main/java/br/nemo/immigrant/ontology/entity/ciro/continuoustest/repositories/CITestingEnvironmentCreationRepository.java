package br.nemo.immigrant.ontology.entity.ciro.continuoustest.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuoustest.models.CITestingEnvironmentCreation;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface CITestingEnvironmentCreationRepository extends PagingAndSortingRepository<CITestingEnvironmentCreation, Long>, ListCrudRepository<CITestingEnvironmentCreation, Long> {

}
