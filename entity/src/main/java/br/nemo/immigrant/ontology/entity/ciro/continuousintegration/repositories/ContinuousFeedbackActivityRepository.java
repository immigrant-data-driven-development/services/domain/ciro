package br.nemo.immigrant.ontology.entity.ciro.continuousintegration.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousintegration.models.ContinuousFeedbackActivity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface ContinuousFeedbackActivityRepository extends PagingAndSortingRepository<ContinuousFeedbackActivity, Long>, ListCrudRepository<ContinuousFeedbackActivity, Long> {

}
