package br.nemo.immigrant.ontology.entity.ciro.continuousbuild.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models.BuildProblem;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface BuildProblemRepository extends PagingAndSortingRepository<BuildProblem, Long>, ListCrudRepository<BuildProblem, Long> {

}
