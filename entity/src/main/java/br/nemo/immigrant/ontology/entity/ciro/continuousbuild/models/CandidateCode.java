package br.nemo.immigrant.ontology.entity.ciro.continuousbuild.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;



import br.nemo.immigrant.ontology.entity.sysswo.system.models.Code;
//import br.nemo.immigrant.ontology.entity.spo.artifact.models.Artifact;
@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "candidatecode")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class CandidateCode extends Code implements Serializable {





  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        CandidateCode elem = (CandidateCode) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "CandidateCode {" +
         "id="+this.id+


      '}';
  }
}
