package br.nemo.immigrant.ontology.entity.ciro.continuoustest.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuoustest.models.AutomatedTestExecution;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface AutomatedTestExecutionRepository extends PagingAndSortingRepository<AutomatedTestExecution, Long>, ListCrudRepository<AutomatedTestExecution, Long> {

}
