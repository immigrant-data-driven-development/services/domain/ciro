package br.nemo.immigrant.ontology.entity.ciro.continuousintegration.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;

import br.nemo.immigrant.ontology.entity.spo.process.models.SpecificProjectProcess;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "continuousintegrationprocess")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class ContinuousIntegrationProcess extends SpecificProjectProcess implements Serializable {




  @ManyToMany(mappedBy = "continuousintegrationprocesss")
  @Builder.Default
  private Set<ContinuousIntegrationServer> continuousintegrationservers = new HashSet<>();

  @ManyToMany(mappedBy = "continuousintegrationprocesss")
  @Builder.Default
  private Set<CITeamStakehoulder> citeamstakehoulders = new HashSet<>();

  @ManyToMany(mappedBy = "continuousintegrationprocesss")
  @Builder.Default
  private Set<CIPersonStakehoulder> cipersonstakehoulders = new HashSet<>();
  
  @Builder.Default
  @Enumerated(EnumType.STRING)
  private ContinuousIntegrationProcessType continuousprocesstype = ContinuousIntegrationProcessType.CHECKINCIPROCESS;


  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        ContinuousIntegrationProcess elem = (ContinuousIntegrationProcess) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "ContinuousIntegrationProcess {" +
         "id="+this.id+

          ", continuousprocesstype='"+this.continuousprocesstype+"'"+
      '}';
  }
}
