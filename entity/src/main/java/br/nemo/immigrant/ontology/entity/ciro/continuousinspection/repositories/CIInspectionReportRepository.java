package br.nemo.immigrant.ontology.entity.ciro.continuousinspection.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.models.CIInspectionReport;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface CIInspectionReportRepository extends PagingAndSortingRepository<CIInspectionReport, Long>, ListCrudRepository<CIInspectionReport, Long> {

}
