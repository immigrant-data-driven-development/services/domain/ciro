package br.nemo.immigrant.ontology.entity.ciro.continuousintegration.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousintegration.models.CIPersonStakehoulder;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface CIPersonStakehoulderRepository extends PagingAndSortingRepository<CIPersonStakehoulder, Long>, ListCrudRepository<CIPersonStakehoulder, Long> {

}
