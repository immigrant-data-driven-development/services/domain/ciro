package br.nemo.immigrant.ontology.entity.ciro.continuousinspection.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.models.AutomatedAdherenceInspection;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface AutomatedAdherenceInspectionRepository extends PagingAndSortingRepository<AutomatedAdherenceInspection, Long>, ListCrudRepository<AutomatedAdherenceInspection, Long> {

}
