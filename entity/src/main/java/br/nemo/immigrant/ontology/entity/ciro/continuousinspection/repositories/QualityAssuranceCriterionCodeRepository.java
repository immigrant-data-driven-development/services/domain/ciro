package br.nemo.immigrant.ontology.entity.ciro.continuousinspection.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.models.QualityAssuranceCriterionCode;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface QualityAssuranceCriterionCodeRepository extends PagingAndSortingRepository<QualityAssuranceCriterionCode, Long>, ListCrudRepository<QualityAssuranceCriterionCode, Long> {

}
