package br.nemo.immigrant.ontology.entity.ciro.continuousintegration.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousintegration.models.ContinuousIntegrationServer;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface ContinuousIntegrationServerRepository extends PagingAndSortingRepository<ContinuousIntegrationServer, Long>, ListCrudRepository<ContinuousIntegrationServer, Long> {

}
