package br.nemo.immigrant.ontology.entity.ciro.continuousinspection.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.models.ContinuousInspectionProcess;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface ContinuousInspectionProcessRepository extends PagingAndSortingRepository<ContinuousInspectionProcess, Long>, ListCrudRepository<ContinuousInspectionProcess, Long> {

}
