package br.nemo.immigrant.ontology.entity.ciro.continuousinspection.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuousinspection.models.CIInspectionEnvironmentCreation;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface CIInspectionEnvironmentCreationRepository extends PagingAndSortingRepository<CIInspectionEnvironmentCreation, Long>, ListCrudRepository<CIInspectionEnvironmentCreation, Long> {

}
