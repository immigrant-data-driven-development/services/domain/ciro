package br.nemo.immigrant.ontology.entity.ciro.continuoustest.repositories;

import br.nemo.immigrant.ontology.entity.ciro.continuoustest.models.ContinuousTestProcess;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface ContinuousTestProcessRepository extends PagingAndSortingRepository<ContinuousTestProcess, Long>, ListCrudRepository<ContinuousTestProcess, Long> {

}
