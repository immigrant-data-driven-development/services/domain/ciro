# 📕Documentation

## 🌀 Project's Package Model
![Domain Diagram](packagediagram.png)

### 📲 Modules
* **[ContinuousIntegration](./continuousintegration/)** :It presents an overview of the CI process, identifying the main events that occur in this context, including continuous build, continuous test, and continuous inspection, each of which is covered in detail in a corresponding subontology
* **[ContinuousBuild](./continuousbuild/)** :It  addresses the main activities, roles, software artifacts, and applications used to implement a continuous and automatic building process in CI context. In additional, this subontology presents the roles that a software artifact performs when it is building.
* **[ContinuousTest](./continuoustest/)** :It  introduces main activities, applications, and software artifacts used to implement a continuous and automatic testing process in the CI context. In addition, this subontology describes the roles that a software artifact under testing can perform in the CI context.
* **[ContinuousInspection](./continuousinspection/)** :It aims at representing main activities, artifacts, roles, and applications involved in a continuous and automatic inspection process in the CI context.

