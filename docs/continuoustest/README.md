# 📕Documentation: ContinuousTest

It  introduces main activities, applications, and software artifacts used to implement a continuous and automatic testing process in the CI context. In addition, this subontology describes the roles that a software artifact under testing can perform in the CI context.

## 🌀 Package's Data Model

![Domain Diagram](classdiagram.png)

### ⚡Entities

* **ContinuousTestProcess** : It is an automated Test Process that verifies whether the new version of the software is in conformance with the software requirements applying automated tests
* **CITestingEnvironmentCreation** : -
* **AutomatedTesting** : -
* **AutomatedTestExecution** : -
* **CITestResult** : -
* **CandidateCodeUnderTest** : -
