# 📕Documentation: ContinuousInspection

It aims at representing main activities, artifacts, roles, and applications involved in a continuous and automatic inspection process in the CI context.

## 🌀 Package's Data Model

![Domain Diagram](classdiagram.png)

### ⚡Entities

* **ContinuousInspectionProcess** : It could be part of a Continuous Integration Process to verify the quality of a software artifact 
* **CIInspectionEnvironmentCreation** : -
* **AutomatedAdherenceInspection** : -
* **AutomatedArtifactInspection** : -
* **QualityAssuranceCriterionCode** : -
* **CandidateCodeUnderInspection** : -
* **CIInspectionReport** : -
