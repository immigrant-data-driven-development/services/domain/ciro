# 📕Documentation: ContinuousBuild

It  addresses the main activities, roles, software artifacts, and applications used to implement a continuous and automatic building process in CI context. In additional, this subontology presents the roles that a software artifact performs when it is building.

## 🌀 Package's Data Model

![Domain Diagram](classdiagram.png)

### ⚡Entities

* **ContinuousBuildProcess** : It is an automated Specific Performed Project Process, which aims at building a new version of the software to be tested.
* **TestCodeCopy** : -
* **SourceCodeCopy** : -
* **BuildEnviromentCreation** : -
* **CodeCheckout** : -
* **CandidateCode** : -
* **CandidateCodeBuilding** : -
* **BuildProblem** : -
