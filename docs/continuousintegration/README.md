# 📕Documentation: ContinuousIntegration

It presents an overview of the CI process, identifying the main events that occur in this context, including continuous build, continuous test, and continuous inspection, each of which is covered in detail in a corresponding subontology

## 🌀 Package's Data Model

![Domain Diagram](classdiagram.png)

### ⚡Entities

* **ContinuousIntegratioServer** : -
* **CITeamStakehoulder** : A CI Stakeholder is a Team Stakeholder (e.g., a developmen and test team) interested in information
(e.g., success or failure of a software artifact during a testing process) about a Continuous
Integration Process.
* **CIPersonStakehoulder** : A CI Stakeholder is a Stakeholder (e.g., a developer and tester) interested in information
(e.g., success or failure of a software artifact during a testing process) about a Continuous
Integration Process.
* **ContinuousIntegrationProcess** : It is an automated process that performs tests in the project’s software artifacts to identify a building problem, a non-compliance with
projects requirements, quality problem, and communicate the stakeholders about success
or failed in any subprocess in an instance of Continuous Integration Process
* **ContinuousFeedbackActivity** : it is an automated Performed Simple Project Activity that provides to a CI Stakeholders information about the status of a CI process 
